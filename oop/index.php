<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new animal("shaun");

    echo "Nama hewan $sheep->name <br>"; // "shaun"
    echo "Jumlah kaki $sheep->legs <br>"; // 4
    echo "Hewan berdarah dingin: $sheep->cold_blooded <br><br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "Nama hewan $sungokong->name <br>"; // "shaun"
    echo "Jumlah kaki $sungokong->legs <br>"; // 4
    $sungokong->yell(); // "Auooo"
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama hewan $kodok->name <br>"; // "shaun"
    echo "Jumlah kaki $kodok->legs <br>"; // 4
    $kodok->jump(); // "hop hop"
?>