<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\gameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// sudah buat branch

Route::get('/', function () {
    return view('layouts.master');
});

//create 
Route::get('/game/create', [gameController::class, 'create']);

//kirim ke database
Route::post('/game', [gameController::class, 'store']);


//Read
//Tampil semua data
Route::get('/game', [gameController::class, 'index']);
//detail kategori berdasarkan id
Route::get('/game/{cast_id}', [gameController::class, 'show']);

//Update
//Form Update
Route::get('/game/{cast_id}/edit', [gameController::class, 'edit']);
//Update data ke database
Route::put('/game/{cast_id}', [gameController::class, 'update']);

//Delete data
//bedasarkan id
Route::delete('/game/{cast_id}', [gameController::class, 'destroy']);
