@extends('layouts.master')

@section('judul')

    Detail Game

@endsection 

@section('content')

    <h3 style="color:blue"> {{$game->nama}} {{$game->year}}</h3>
    <p>Developer : {{$game->developer}}</p>
    <h5 style="color:black">Gamplay </h5>
    <p>{{$game->gameplay}}</p>
    

    <a href="/game" class="btn btn-secondary btn-sm">Kembali</a>

@endsection