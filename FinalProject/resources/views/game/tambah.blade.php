@extends('layouts.master')

@section('judul')

    Halaman Tambah Data Game

@endsection 

@section('content')

<form action="/game" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Gameplay</label>
        <input type="text" name="gameplay" class="form-control">
      </div>
      @error('gameplay')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label>Developer</label>
      <input type="text" name="developer" class="form-control">
    </div>
    @error('developer')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Year</label>
        <input type="text" name="year" class="form-control">
    </div>
    @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Memasangkan script sweet alert",
            icon: "success",
            confirmButtonText: "Cool",
        });
    </script>
    
    <button type="submit" class="btn btn-primary " >Submit</button>
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Memasangkan script sweet alert",
            icon: "success",
            confirmButtonText: "Cool",
        });
    </script>
    
  </form>

@endsection