<!DOCTYPE html>
<html>
<head>
<title>Sign Up</title>
</head>
<body>

<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>

<br>

<form action="/kirim" method="post">
    @csrf
    <label>First Name:</label>
    <br>
    <input type="text" name="name"><br>
    <br>
    <label>Last Name:</label>
    <br>
    <input type="text" name="lastname"><br>
    <br>
    <label>Gender:</label>
    <br>
    <input type="radio" id="male" name="gender" value="Male">Male<br>
    <input type="radio" id="female" name="gender" value="Female">Female<br>
    <br>
    <label>Nationality:</label>
    <br>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="italy">Italy</option>
        <option value="jepang">Jepang</option>
    </select><br>
    <br>
    <label>Language Spoken:</label>
    <br>
    <input type="checkbox" name="language">Bahasa Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Other<br>
    <br>
    <label>Bio:</label>
    <br>
    <textarea name="massage" rows="10" cols="30"></textarea>
    <br><br>
    <input type="submit" value="kirim">
</form>


</body>
</html>
 