<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('Register');
    }

    public function kirim(Request $request)
    {
        $nama = $request ['name'];
        $nama_belakang = $request ['lastname'];
        
        return view ('Welcome', ['nama' => $nama, 'nama_belakang' => $nama_belakang]);
    }
}
