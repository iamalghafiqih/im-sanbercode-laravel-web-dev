@extends('layout.master')

@section('judul')

Halaman List Cast
    
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm mb-3">
        Tambah
    </a>

    <div class="card">
    
        <!-- /.card-header -->
        <div class="card-body">
          <table id="myTable" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Umur</th>
              <th>Bio</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key => $value)
                   <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>{{$value->bio}}</td>
                    <td>
                        <form action="/cast/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/cast/{{$value->id}}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                   </tr>
                @empty
                    <p>No Data</p>
                @endforelse
            </tbody>
          </table>
        </div>
@endsection