@extends('layout.master')

@section('judul')

    Detail Cast

@endsection 

@section('content')

    <p>Nama : {{$cast->nama}}</p>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection