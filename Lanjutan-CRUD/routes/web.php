<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

// Route::get('/', [AuthController::class, 'registrasi']);

// Route::get('/table', [AuthController::class, 'bio']);

// Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/table', function(){
    return view('table1');
});


Route::get('/data-table', function(){
    return view('table');
});

//create 
Route::get('/cast/create', [CastController::class, 'create']);

//kirim ke database
Route::post('/cast', [CastController::class, 'store']);


//Read
//Tampil semua data
Route::get('/cast', [CastController::class, 'index']);
//detail kategori berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update
//Form Update
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Update data ke database
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete data
//bedasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);